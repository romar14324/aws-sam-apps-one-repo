# AWS - SAM APPS ONE REPO BOILERPLATE

Configure a single AWS API Gateway with multiple AWS Lambda Functions in one Repository..

- Run SAM in local machine
- Deploy API Gateway and Lambda Function

## Installation

Prerequisites - Needed to be installed on local machine:

  - [x]  [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
  - [x]  Configure AWS CLI
  - [x]  [Install SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-sam-cli.html)
  - [x]  [Install Docker (Desktop)](https://docs.docker.com/engine/install/)
  - [x]  Python 3.9

## Configure AWS CLI

- Add IAM User Account in order to get the `AWS Access Key ID` and `AWS Secret Access Key`.
    - AWS Console > IAM > Users > Add User
    - Select AWS Credential Type: "Access key - Programmatic access"
    - Set of Permissions:
        - IAMFullAccess
        - AmazonS3FullAccess
        - AWSCloudFormationFullAccess
        - AmazonAPIGatewayAdministrator
        - CloudWatchFullAccess
        - AmazonDynamoDBFullAccess
        - AWSLambda_FullAccess

- Don't forget to get your `AWS Access Key ID (AWS_ACCESS_KEY_ID)` and `AWS Secret Access Key (AWS_SECRET_ACCESS_KEY)`.

![IAM Permissions Policies](readme_assets/IAM.png)

- If you already have your Security Credentials and AWS CLI Installed, type this aws command on your terminal:

  ```bash
  aws configure
  ```
  Then, enter your `AWS Access Key ID` and `AWS Secret Access Key`



## Run Locally

#### Clone the project

```bash
  git clone https://gitlab.com/romar14324/aws-sam-apps-one-repo.git
```

#### Go to the project directory

```bash
  cd aws-sam-apps-one-repo
```

#### Run Virtual Environment (For python 3.x version)

```bash
  python3 -m venv myenv
  source myenv/bin/activate
```

#### Install dependencies
- Make sure you are inside your `lambda function` folder to reference requirements.txt file

```bash
  cd lambda2
  pip install -r requirements.txt
```

If you want to deactivate your Virtual Environment:

```bash
  deactivate
```

## 2 Options to Run your Functions Locally:

- Invoke only your function locally:

    ```bash
    sam local invoke -e ./events/event.json HelloWorldFunction
    ```
    *"HelloWorldFunction" is your function name defined in template.yml*

- Start your local api server:

    ```bash
    sam local start-api
    ```
    *SAM will automatically look for ".yml" configuration file.*


## Local Deployment

Build and Deploy Project

```bash
  sam build
  sam deploy --guided
```

- Go to AWS CloudFormation and look for the deployment "stack" that SAM automatically deployed it for you.

When using the ff. SAM CLI commands:
- `sam build` command - SAM will build your project and install dependencies based on requirements.txt.
- `sam deploy --guided` - SAM will deploy the API Gateway and Lambda functions, as well as any dependencies.


## Reference:

- [AWS SAM Tutorial (with a Lambda Example!)](https://www.youtube.com/watch?v=MipjLaTp5nA)
- [How To Test your AWS Lambda Locally with SAM](https://www.youtube.com/watch?v=AUQRyl1SNcU&t=39s)
- [SAM template: Configure an api gateway with multiple lambdas](https://manuchandrasekhar.medium.com/sam-template-configure-an-api-gateway-with-multiple-lambdas-921b38873389)
- [sam-app boilerplate](https://github.com/beabetterdevv/sam-app)

